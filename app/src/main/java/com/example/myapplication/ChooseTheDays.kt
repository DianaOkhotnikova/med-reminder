package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.CheckBox
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.db.DaysEnum
import com.example.myapplication.room.DbObjects

class ChooseTheDays : AppCompatActivity()  {
    lateinit var button: ImageButton
    lateinit var sunday: CheckBox
    lateinit var monday: CheckBox
    lateinit var tuesday: CheckBox
    lateinit var wednesday: CheckBox
    lateinit var thursday: CheckBox
    lateinit var friday: CheckBox
    lateinit var saturday: CheckBox
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.choosethedays)
        button = findViewById(R.id.btnName)
        sunday = findViewById(R.id.Sunday)
        monday = findViewById(R.id.Monday)
        tuesday = findViewById(R.id.Tuesday)
        wednesday = findViewById(R.id.Wednesday)
        thursday = findViewById(R.id.Thursday)
        friday = findViewById(R.id.Friday)
        saturday = findViewById(R.id.Saturday)
        @Suppress("DEPRECATION")
        val objectExtra = intent.getSerializableExtra("objects") as DbObjects
        println(objectExtra)
        if (objectExtra.flagUpdate){
            if (objectExtra.days == DaysEnum.Sunday) {
                sunday.isChecked = true
            } else if (objectExtra.days == DaysEnum.Monday) {
                monday.isChecked = true
            } else if (objectExtra.days == DaysEnum.Tuesday) {
                tuesday.isChecked = true
            } else if (objectExtra.days == DaysEnum.Wednesday) {
                wednesday.isChecked = true
            } else if (objectExtra.days == DaysEnum.Thursday) {
                thursday.isChecked = true
            } else if (objectExtra.days == DaysEnum.Friday) {
                friday.isChecked = true
            } else if (objectExtra.days == DaysEnum.Saturday) {
                saturday.isChecked = true
            }
        }
        button.setOnClickListener {
            if (sunday.isChecked) {
                objectExtra.days = DaysEnum.Sunday
            }
            if (monday.isChecked) {
                objectExtra.days = DaysEnum.Monday
            }
            if (tuesday.isChecked) {
                objectExtra.days = DaysEnum.Tuesday
            }
            if (wednesday.isChecked) {
                objectExtra.days = DaysEnum.Wednesday
            }
            if (thursday.isChecked) {
                objectExtra.days = DaysEnum.Thursday
            }
            if (friday.isChecked) {
                objectExtra.days = DaysEnum.Friday
            }
            if (saturday.isChecked) {
                objectExtra.days = DaysEnum.Saturday
            }
            val intent: Intent = Intent(
                this,
                FirstDose::class.java
            )
            intent.putExtra("objects", objectExtra)
            startActivity(intent)
        }
        button = findViewById(R.id.imageButton11)
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                Often::class.java
            )
            intent.putExtra("objects", objectExtra)
            startActivity(intent)
        }
    }
}