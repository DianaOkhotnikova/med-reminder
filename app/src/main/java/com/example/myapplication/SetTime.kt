package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.myapplication.room.DBHandler
import com.example.myapplication.room.DbObjects
import com.example.myapplication.room.MedecineTable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class SetTime : AppCompatActivity()  {
    lateinit var button: ImageButton
    lateinit var timer: TimePicker
    val db by  lazy { DBHandler.getDb(this) }
    //val manager by lazy { DbMannager.getInstance(this)}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.set_time)
        button = findViewById(R.id.btnName)
        timer = findViewById(R.id.timePicker)
        @Suppress("DEPRECATION")
        val setTime = intent.getSerializableExtra("objects") as DbObjects
        println(setTime)
        if (setTime.flagUpdate){
            var pars  = setTime.time.split(":")
            timer.hour = pars[0].toInt()
            timer.minute = pars[1].toInt()
        }
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                HomeActivity::class.java
            )
            setTime.time = timer.hour.toString() + ":" + timer.minute.toString()
            val medTab = MedecineTable(
                medicine = setTime.medicine,
                kind = setTime.kind.toString(),
                strength = setTime.strength,
                purpose = setTime.purpose,
                often = setTime.often.toString(),
                days = setTime.days.toString(),
                firstdose = setTime.firstdose,
                time = setTime.time,
            )
            if (setTime.flagUpdate){
                lifecycleScope.launch(Dispatchers.IO) {
                    db.medecineDao.updateDB(
                        setTime.id,
                        medTab.medicine,
                        medTab.kind,
                        medTab.strength,
                        medTab.purpose,
                        medTab.often,
                        medTab.days,
                        medTab.firstdose,
                        medTab.time
                    )
                }
                startActivity(intent)
            }else{
                lifecycleScope.launch(Dispatchers.IO) {
                    db.medecineDao.insertDB(medTab)
                }
            }
            startActivity(intent)
        }
        button = findViewById(R.id.imageButton11)
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                FirstDose::class.java
            )
            intent.putExtra("objects", setTime)
            startActivity(intent)
        }
    }
}