package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.db.DaysEnum
import com.example.myapplication.db.KindEnum
import com.example.myapplication.db.OftenEnum
import com.example.myapplication.room.DBHandler
import com.example.myapplication.room.DbObjects
import com.example.myapplication.widgets.RecyclerAdapter
import com.google.android.material.navigation.NavigationBarView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class HomeActivity : AppCompatActivity() {
    lateinit var button: ImageButton
    lateinit var textName1: TextView

    val db by  lazy { DBHandler.getDb(this) }
    private val mOnNavigationItemSelectedListener=
        NavigationBarView.OnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.home -> {
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    true
                }
                R.id.guide -> {
                    val intent = Intent(this, Guide::class.java)
                    startActivity(intent)
                    true
                }
                R.id.medications -> {
                    val intent = Intent(this, Medications::class.java)
                    startActivity(intent)
                    true
                }
                R.id.profile -> {
                    val intent = Intent(this, Profile::class.java)
                    startActivity(intent)
                    true
                }
            }
            false
        }


    private lateinit var getObj: List<DbObjects>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_page)
        findViewById<NavigationBarView>(R.id.bottomNavigationView).setOnItemSelectedListener(
            mOnNavigationItemSelectedListener
        )
        button = findViewById(R.id.imageButton9)
        val editor = getSharedPreferences("SETTING", MODE_PRIVATE)
        textName1 = findViewById(R.id.textView4)
        textName1.setText("${editor.getString("name", null)}")


        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        var iter = 0
        getObj = mutableListOf()
        val thiss = this
        lifecycleScope.launch (Dispatchers.IO) {
            val getTable = db.medecineDao.readTable()
            getTable.forEach {
                getObj += DbObjects()
                getObj[iter].id = it.id
                getObj[iter].medicine = it.medicine
                getObj[iter].kind = KindEnum.valueOf(it.kind)
                getObj[iter].strength = it.strength
                getObj[iter].purpose = it.purpose
                getObj[iter].often = OftenEnum.valueOf(it.often)
                getObj[iter].days = DaysEnum.valueOf(it.days)
                getObj[iter].firstdose = it.firstdose
                getObj[iter].time = it.time
                iter++
            }
            thiss.runOnUiThread {
                recyclerView.adapter = RecyclerAdapter(getObj, thiss)
            }
        }
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                AddMedication::class.java
            )
            startActivity(intent)
        }

    }
    fun onClickDeleted(dbObjects: DbObjects){
        val intent: Intent = Intent(
            this,
            DeleteMedecine::class.java
        )
        intent.putExtra("cardObject", dbObjects)
        println(dbObjects)
        startActivity(intent)
    }
}