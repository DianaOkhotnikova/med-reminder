package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.room.DbObjects
import com.google.android.material.textfield.TextInputEditText

class Purpose : AppCompatActivity()  {
    lateinit var button: ImageButton
    lateinit var inputPurpose: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.purpose)
        button = findViewById(R.id.btnName)
        inputPurpose = findViewById(R.id.inputPurpose)
        @Suppress("DEPRECATION")
        val namePurpose = intent.getSerializableExtra("objects") as DbObjects
        println(namePurpose)
        if (namePurpose.flagUpdate){
            inputPurpose.setText(namePurpose.purpose)
        }
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                Often::class.java
            )
            namePurpose.purpose = inputPurpose.text.toString()
            intent.putExtra("objects", namePurpose)
            startActivity(intent)
        }
        button = findViewById(R.id.imageButton11)
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                StrengthMed::class.java
            )
            intent.putExtra("objects", namePurpose)
            startActivity(intent)
        }
    }
}