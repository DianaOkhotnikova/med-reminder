package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.myapplication.room.DBHandler
import com.example.myapplication.room.DbObjects
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DeleteMedecine : AppCompatActivity() {
    //val manager by lazy { DbMannager.getInstance(this)}
    val db by  lazy { DBHandler.getDb(this) }
    lateinit var button: ImageButton
    lateinit var time: TextView
    lateinit var medecine: TextView
    lateinit var kind: TextView
    lateinit var cardFirstDose: TextView
    lateinit var buttonTresh: ImageButton
    lateinit var buttonEdit: ImageButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_medication)
        time = findViewById(R.id.cardTime)
        medecine = findViewById(R.id.cardMedecine)
        kind = findViewById(R.id.cardKind)
        cardFirstDose = findViewById(R.id.cardFirstDose)
        buttonTresh = findViewById(R.id.tresh)
        buttonEdit = findViewById(R.id.imageButtonEdit)
        @Suppress("DEPRECATION")
        val objectExtra = intent.getSerializableExtra("cardObject") as DbObjects
        time.text = objectExtra.time
        medecine.text = objectExtra.medicine
        kind.text = objectExtra.kind.toString()
        val pars = objectExtra.firstdose.split("/")
        cardFirstDose.text = pars[0]
        button = findViewById(R.id.x_circle)
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                HomeActivity::class.java
            )
            startActivity(intent)
        }
        buttonTresh.setOnClickListener {
            val intent: Intent = Intent(
                this,
                HomeActivity::class.java
            )
//            manager.openDb()
//            manager.deleteDb(objectExtra)
            lifecycleScope.launch(Dispatchers.IO) {
                db.medecineDao.deleteDB(objectExtra.id)
            }
            startActivity(intent)
        }
        buttonEdit.setOnClickListener {
            val intent: Intent = Intent(
                this,
                AddMedication::class.java
            )
            intent.putExtra("editFlag", true)
            intent.putExtra("obj", objectExtra)
            startActivity(intent)
        }
    }
}