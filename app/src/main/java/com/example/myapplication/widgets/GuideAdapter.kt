package com.example.myapplication.widgets

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import android.widget.TextView
import android.widget.VideoView
import androidx.media3.ui.PlayerView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.Guide
import com.example.myapplication.R
import com.example.myapplication.room.GuideTable

class GuideAdapter(private val dataList: List<GuideTable>, private val activity: Guide) : RecyclerView.Adapter<GuideAdapter.MyViewHolder>() {


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val titleGuide: TextView = itemView.findViewById(R.id.titleGuide)
        val videoGuide: VideoView = itemView.findViewById(R.id.videoGuide)
        init {
            val mediaController = MediaController(activity)
            videoGuide.setMediaController(mediaController)
            mediaController.setMediaPlayer(videoGuide)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_guide, parent, false)
        return MyViewHolder(itemView)
    }

    private fun initializePlayer(videoView: VideoView, videoUrl: String) {
        val uri = Uri.parse(videoUrl)
        videoView.setVideoURI(uri)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = dataList[position]
        holder.titleGuide.text = currentItem.question
        initializePlayer(holder.videoGuide, currentItem.video)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}

