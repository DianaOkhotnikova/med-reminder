package com.example.myapplication.widgets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.HomeActivity
import com.example.myapplication.Medications
import com.example.myapplication.R
import com.example.myapplication.room.DbObjects

class MedecationAdapter(private val dataList: List<DbObjects>, private val activity: Medications) : RecyclerView.Adapter<MedecationAdapter.MyViewHolder>() {
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val timeView: TextView = itemView.findViewById(R.id.textView9)
        val medecineView: TextView = itemView.findViewById(R.id.textView10)
        val pillView: TextView = itemView.findViewById(R.id.textView11)
        val foodView: TextView = itemView.findViewById(R.id.textView12)
        init {
            val deleteMedecine1 = itemView.findViewById<ImageButton>(R.id.medecineButton)
            deleteMedecine1.setOnClickListener{
                activity.onClickDeleted(dataList[absoluteAdapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_home_page, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = dataList[position]
        val pars = currentItem.firstdose.split('/')
        holder.timeView.text = currentItem.time
        holder.medecineView.text = currentItem.medicine
        holder.pillView.text = currentItem.kind.toString()
        holder.foodView.text = pars[0]
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}