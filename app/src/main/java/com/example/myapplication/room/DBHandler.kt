package com.example.myapplication.room

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.myapplication.R
import kotlin.concurrent.thread

object DBHandler {

    @Volatile var INSTANCE: MedecineDB? = null

    fun getInstance(context: Context): MedecineDB = INSTANCE ?: synchronized(this) {
        INSTANCE ?: getDb(context).also { INSTANCE = it }
    }

    fun  getDb(context: Context): MedecineDB{
        val db = Room.databaseBuilder(context, MedecineDB::class.java, "MedecineDB").addCallback(
            object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    thread {
                        getInstance(context).medecineDao.insertGuide(GUIDE_DATA)
                    }
                }
            }).build()
        return db
    }

    val GUIDE_DATA = listOf(
        GuideTable("How to add new medicine ?","android.resource://"+ "com.example.Guide" +"/${R.raw.guide}"),
        GuideTable("How to share with others ?","android.resource://raw/${R.raw.guide}"),
        GuideTable("How to book appointments?","android.resource://raw/${R.raw.guide}"),
    )
}