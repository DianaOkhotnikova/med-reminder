package com.example.myapplication.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MedecineTable (
    var medicine: String ,
    var kind: String,
    var strength: String,
    var purpose: String,
    var often: String,
    var days: String,
    var firstdose: String,
    var time: String,

    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
)