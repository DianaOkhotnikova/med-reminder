package com.example.myapplication.room

import com.example.myapplication.db.DaysEnum
import com.example.myapplication.db.KindEnum
import com.example.myapplication.db.OftenEnum
import java.io.Serializable

data class DbObjects (
    var medicine: String = "",
    var kind: KindEnum = KindEnum.Drops,
    var strength: String = "",
    var purpose: String = "",
    var often: OftenEnum = OftenEnum.Daily,
    var days: DaysEnum = DaysEnum.Friday,
    var firstdose: String = "",
    var time: String = "",
    var flagUpdate: Boolean = false,
    var id: Int = 0
) : Serializable {}