package com.example.myapplication.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class GuideTable (
    var question: String ,
    var video: String,

    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
)