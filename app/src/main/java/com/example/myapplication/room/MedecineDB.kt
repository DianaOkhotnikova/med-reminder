package com.example.myapplication.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [MedecineTable::class, GuideTable::class],
    version = 1,
)
abstract class MedecineDB :  RoomDatabase(){

    abstract val medecineDao:MedecineDao

}