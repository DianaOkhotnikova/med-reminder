package com.example.myapplication.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MedecineDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertDB(medecineTable: MedecineTable)

    @Query("SELECT * FROM 'MedecineTable'")
    fun readTable(): List<MedecineTable>

    @Query("DELETE FROM 'MedecineTable' WHERE id = :medID")
    fun deleteDB(medID: Int)

    @Query("UPDATE 'MedecineTable' SET medicine = :med, kind = :kin, strength = :stre, purpose = :pur, often = :oft, days = :day, firstdose = :first, time = :ti WHERE id = :medID")
    fun updateDB(medID: Int, med: String, kin:String, stre:String, pur:String, oft:String, day:String, first:String, ti: String)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertGuide(GuideTable: List<GuideTable>)

    @Query("SELECT * FROM 'GuideTable'")
    fun readGuide(): List<GuideTable>

    @Query("DELETE FROM 'GuideTable' WHERE id = :medID")
    fun deleteGuide(medID: Int)

    @Query("UPDATE 'GuideTable' SET question = :que, video = :vid WHERE id = :medID")
    fun updateGuide(que: String, vid: String, medID: Int)

}