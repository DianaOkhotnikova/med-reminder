package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.CheckBox
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.room.DbObjects
import com.example.myapplication.db.KindEnum


class ThreePageActivity : AppCompatActivity()  {
    lateinit var button: ImageButton
    lateinit var Pill: CheckBox
    lateinit var Solution: CheckBox
    lateinit var Injection: CheckBox
    lateinit var Powder: CheckBox
    lateinit var Drops: CheckBox
    lateinit var Inhaler: CheckBox
    lateinit var Other: CheckBox

    private fun click(checkBox1: CheckBox, checkBox2: CheckBox, checkBox3: CheckBox, checkBox4: CheckBox, checkBox5: CheckBox, checkBox6: CheckBox, ){
        checkBox1.isChecked = false
        checkBox2.isChecked = false
        checkBox3.isChecked = false
        checkBox4.isChecked = false
        checkBox5.isChecked = false
        checkBox6.isChecked = false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.three_page)
        button = findViewById(R.id.btnName)
        Pill = findViewById(R.id.checkBox1)
        Solution = findViewById(R.id.checkBox2)
        Injection = findViewById(R.id.checkBox3)
        Powder = findViewById(R.id.checkBox4)
        Drops = findViewById(R.id.checkBox5)
        Inhaler = findViewById(R.id.checkBox6)
        Other = findViewById(R.id.checkBox7)
        @Suppress("DEPRECATION")
        val nameMedicene = intent.getSerializableExtra("objects") as DbObjects
        println(nameMedicene)
        Pill.setOnClickListener {
            click(Solution, Injection, Powder, Drops, Inhaler, Other)
        }
        Solution.setOnClickListener {
            click(Pill, Injection, Powder, Drops, Inhaler, Other)
        }
        Injection.setOnClickListener {
            click(Solution, Pill, Powder, Drops, Inhaler, Other)
        }
        Powder.setOnClickListener {
            click(Solution, Injection, Pill, Drops, Inhaler, Other)
        }
        Drops.setOnClickListener {
            click(Solution, Injection, Powder, Pill, Inhaler, Other)
        }
        Inhaler.setOnClickListener {
            click(Solution, Injection, Powder, Drops, Pill, Other)
        }
        Other.setOnClickListener {
            click(Solution, Injection, Powder, Drops, Inhaler, Pill)
        }
        if(nameMedicene.flagUpdate){
            if (nameMedicene.kind == KindEnum.Pill) {
                Pill.isChecked = true
            } else if (nameMedicene.kind == KindEnum.Solution) {
                Solution.isChecked = true
            } else if (nameMedicene.kind == KindEnum.Injection) {
                Injection.isChecked = true
            } else if (nameMedicene.kind == KindEnum.Powder) {
                Powder.isChecked = true
            } else if (nameMedicene.kind == KindEnum.Drops) {
                Drops.isChecked = true
            } else if (nameMedicene.kind == KindEnum.Inhaler) {
                Inhaler.isChecked = true
            } else if (nameMedicene.kind == KindEnum.Other) {
                Other.isChecked = true
            }
        }
        button.setOnClickListener {
            if (Pill.isChecked) {
                nameMedicene.kind = KindEnum.Pill
            }
            if (Solution.isChecked) {
                nameMedicene.kind = KindEnum.Solution
            }
            if (Injection.isChecked) {
                nameMedicene.kind = KindEnum.Injection
            }
            if (Powder.isChecked) {
                nameMedicene.kind = KindEnum.Powder
            }
            if (Drops.isChecked) {
                nameMedicene.kind = KindEnum.Drops
            }
            if (Inhaler.isChecked) {
                nameMedicene.kind = KindEnum.Inhaler
            }
            if (Other.isChecked) {
                nameMedicene.kind = KindEnum.Other
            }
            val intent: Intent = Intent(
                this,
                StrengthMed::class.java
            )
            intent.putExtra("objects", nameMedicene)
            startActivity(intent)
        }
        button = findViewById(R.id.imageButton11)
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                AddMedication::class.java
            )
            startActivity(intent)
        }
    }
}