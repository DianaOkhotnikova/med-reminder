package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.CheckBox
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.room.DbObjects
import com.google.android.material.textfield.TextInputEditText

class StrengthMed : AppCompatActivity()  {
    lateinit var button: ImageButton
    lateinit var srength: TextInputEditText
    lateinit var g: CheckBox
    lateinit var iU: CheckBox
    lateinit var mCG: CheckBox
    lateinit var mEG: CheckBox
    lateinit var mG: CheckBox

    private fun click(checkBox1: CheckBox, checkBox2: CheckBox, checkBox3: CheckBox, checkBox4: CheckBox,){
        checkBox1.isChecked = false
        checkBox2.isChecked = false
        checkBox3.isChecked = false
        checkBox4.isChecked = false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.strength_medicine)
        button = findViewById(R.id.btnName)
        srength = findViewById(R.id.inputStrength)
        g = findViewById(R.id.checkBoxG)
        iU = findViewById(R.id.checkBoxIU)
        mCG = findViewById(R.id.checkBox6MCG)
        mEG = findViewById(R.id.checkBoxMEG)
        mG = findViewById(R.id.checkBoxMG)
        @Suppress("DEPRECATION")
        val nameThree = intent.getSerializableExtra("objects") as DbObjects
        println(nameThree)
        g.setOnClickListener {
            click(iU, mCG, mEG, mG)
        }
        iU.setOnClickListener {
            click(g, mCG, mEG, mG)
        }
        mCG.setOnClickListener {
            click(g, iU, mEG, mG)
        }
        mEG.setOnClickListener {
            click(g, iU, mCG, mG)
        }
        mG.setOnClickListener {
            click(g, iU, mCG, mEG)
        }
        if (nameThree.flagUpdate){
            val parts = nameThree.strength.split(" ")
            srength.setText(parts[0])
            if (nameThree.strength.contains(mG.text.toString())){
                mG.isChecked = true
            } else if (nameThree.strength.contains(iU.text.toString())){
                iU.isChecked = true
            } else if (nameThree.strength.contains(mCG.text.toString())){
                mCG.isChecked = true
            } else if (nameThree.strength.contains(mEG.text.toString())){
                mEG.isChecked = true
            } else if (nameThree.strength.contains(g.text.toString())){
                g.isChecked = true
            }
        }
        button.setOnClickListener {
            if (g.isChecked) {
                nameThree.strength = srength.text.toString() + " " + g.text.toString()
            }
            if (iU.isChecked) {
                nameThree.strength  = srength.text.toString() + " " + iU.text.toString()
            }
            if (mCG.isChecked) {
                nameThree.strength  = srength.text.toString() + " " + mCG.text.toString()
            }
            if (mEG.isChecked) {
                nameThree.strength  = srength.text.toString() + " " + mEG.text.toString()
            }
            if (mG.isChecked) {
                nameThree.strength  = srength.text.toString() + " " + mG.text.toString()
            }
            val intent: Intent = Intent(
                this,
                Purpose::class.java
            )
            intent.putExtra("objects", nameThree)
            startActivity(intent)
        }
        button = findViewById(R.id.imageButton11)
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                ThreePageActivity::class.java
            )
            intent.putExtra("objects", nameThree)
            startActivity(intent)
        }
    }
}