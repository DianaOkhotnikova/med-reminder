package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.CheckBox
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.room.DbObjects
import com.example.myapplication.db.OftenEnum

class Often : AppCompatActivity()  {
    lateinit var button: ImageButton
    lateinit var daily: CheckBox
    lateinit var onceAWeek: CheckBox
    lateinit var twoDaysAWeek: CheckBox
    lateinit var threeDaysAWeek: CheckBox
    lateinit var fourDaysAWeek: CheckBox
    lateinit var fiveDaysAWeek: CheckBox
    lateinit var sixDaysAWeek: CheckBox
    lateinit var onceAMonth: CheckBox
    lateinit var alternateDays: CheckBox

    private fun click(checkBox1: CheckBox, checkBox2: CheckBox, checkBox3: CheckBox, checkBox4: CheckBox,checkBox5: CheckBox,
                      checkBox6: CheckBox, checkBox7: CheckBox,checkBox8: CheckBox,){
        checkBox1.isChecked = false
        checkBox2.isChecked = false
        checkBox3.isChecked = false
        checkBox4.isChecked = false
        checkBox5.isChecked = false
        checkBox6.isChecked = false
        checkBox7.isChecked = false
        checkBox8.isChecked = false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.often)
        button = findViewById(R.id.btnName)
        daily = findViewById(R.id.Daily)
        onceAWeek = findViewById(R.id.OnceAWeek)
        twoDaysAWeek = findViewById(R.id.TwoDaysAWeek)
        threeDaysAWeek = findViewById(R.id.ThreeDaysAWeek)
        fourDaysAWeek = findViewById(R.id.FourDaysAWeek)
        fiveDaysAWeek = findViewById(R.id.FiveDaysAWeek)
        sixDaysAWeek = findViewById(R.id.SixDaysAWeek)
        onceAMonth = findViewById(R.id.OnceAMonth)
        alternateDays = findViewById(R.id.AlternateDays)
        @Suppress("DEPRECATION")
        val often = intent.getSerializableExtra("objects") as DbObjects
        println(often)
        daily.setOnClickListener {
            click(onceAWeek, twoDaysAWeek, threeDaysAWeek, fourDaysAWeek, fiveDaysAWeek, sixDaysAWeek, onceAMonth, alternateDays)
        }
        onceAWeek.setOnClickListener {
            click(daily, twoDaysAWeek, threeDaysAWeek, fourDaysAWeek, fiveDaysAWeek, sixDaysAWeek, onceAMonth, alternateDays)
        }
        twoDaysAWeek.setOnClickListener {
            click(daily, onceAWeek, threeDaysAWeek, fourDaysAWeek, fiveDaysAWeek, sixDaysAWeek, onceAMonth, alternateDays)
        }
        threeDaysAWeek.setOnClickListener {
            click(daily, onceAWeek, twoDaysAWeek, fourDaysAWeek, fiveDaysAWeek, sixDaysAWeek, onceAMonth, alternateDays)
        }
        fourDaysAWeek.setOnClickListener {
            click(daily, onceAWeek, twoDaysAWeek, threeDaysAWeek, fiveDaysAWeek, sixDaysAWeek, onceAMonth, alternateDays)
        }
        fiveDaysAWeek.setOnClickListener {
            click(daily, onceAWeek, twoDaysAWeek, threeDaysAWeek, fourDaysAWeek, sixDaysAWeek, onceAMonth, alternateDays)
        }
        sixDaysAWeek.setOnClickListener {
            click(daily, onceAWeek, twoDaysAWeek, threeDaysAWeek, fourDaysAWeek, fiveDaysAWeek, onceAMonth, alternateDays)
        }
        onceAMonth.setOnClickListener {
            click(daily, onceAWeek, twoDaysAWeek, threeDaysAWeek, fourDaysAWeek, fiveDaysAWeek, sixDaysAWeek, alternateDays)
        }
        alternateDays.setOnClickListener {
            click(daily, onceAWeek, twoDaysAWeek, threeDaysAWeek, fourDaysAWeek, fiveDaysAWeek, sixDaysAWeek, onceAMonth)
        }
        if (often.flagUpdate){
            if (often.often == OftenEnum.Daily) {
                daily.isChecked = true
            } else if (often.often == OftenEnum.OnceAWeek) {
                onceAWeek.isChecked = true
            } else if (often.often == OftenEnum.TwoDaysAWeek) {
                twoDaysAWeek.isChecked = true
            } else if (often.often == OftenEnum.ThreeDaysAWeek) {
                threeDaysAWeek.isChecked = true
            } else if (often.often == OftenEnum.FourDaysAWeek) {
                fourDaysAWeek.isChecked = true
            } else if (often.often == OftenEnum.FiveDaysAWeek) {
                fiveDaysAWeek.isChecked = true
            } else if (often.often == OftenEnum.SixDaysAWeek) {
                sixDaysAWeek.isChecked = true
            } else if (often.often == OftenEnum.OnceAMonth) {
                onceAMonth.isChecked = true
            } else if (often.often == OftenEnum.AlternateDays) {
                alternateDays.isChecked = true
            }
        }
        button.setOnClickListener {
            if (daily.isChecked) {
                often.often = OftenEnum.Daily
            }
            if (onceAWeek.isChecked) {
                often.often = OftenEnum.OnceAWeek
            }
            if (twoDaysAWeek.isChecked) {
                often.often = OftenEnum.TwoDaysAWeek
            }
            if (threeDaysAWeek.isChecked) {
                often.often = OftenEnum.ThreeDaysAWeek
            }
            if (fourDaysAWeek.isChecked) {
                often.often = OftenEnum.FourDaysAWeek
            }
            if (fiveDaysAWeek.isChecked) {
                often.often = OftenEnum.FiveDaysAWeek
            }
            if (sixDaysAWeek.isChecked) {
                often.often = OftenEnum.SixDaysAWeek
            }
            if (onceAMonth.isChecked) {
                often.often = OftenEnum.OnceAMonth
            }
            if (alternateDays.isChecked) {
                often.often = OftenEnum.AlternateDays
            }
            val intent: Intent = Intent(
                this,
                ChooseTheDays::class.java
            )
            intent.putExtra("objects", often)
            startActivity(intent)
        }
        button = findViewById(R.id.imageButton11)
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                Purpose::class.java
            )
            intent.putExtra("objects", often)
            startActivity(intent)
        }
    }
}