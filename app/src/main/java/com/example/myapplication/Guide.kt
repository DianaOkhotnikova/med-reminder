package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.room.DBHandler
import com.example.myapplication.widgets.GuideAdapter
import com.example.myapplication.widgets.RecyclerAdapter
import com.google.android.material.navigation.NavigationBarView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class Guide : AppCompatActivity() {
    lateinit var button: ImageButton
    private val db by lazy { DBHandler.getInstance(this) }

    private val mOnNavigationItemSelectedListener=
        NavigationBarView.OnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.home -> {
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    true
                }
                R.id.guide -> {
                    val intent = Intent(this, Guide::class.java)
                    startActivity(intent)
                    true
                }
                R.id.medications -> {
                    val intent = Intent(this, Medications::class.java)
                    startActivity(intent)
                    true
                }
                R.id.profile -> {
                    val intent = Intent(this, Profile::class.java)
                    startActivity(intent)
                    true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.guide)
        findViewById<NavigationBarView>(R.id.bottomNavigationView).setOnItemSelectedListener(
            mOnNavigationItemSelectedListener
        )
        button = findViewById(R.id.GuideButtonBack)
        val recyclerView: RecyclerView = findViewById(R.id.guideRecycle)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val thiss = this
        lifecycleScope.launch (Dispatchers.IO) {
            val readGuideDb = db.medecineDao.readGuide()
            thiss.runOnUiThread {
                recyclerView.adapter = GuideAdapter(readGuideDb, thiss)
            }
        }
        button.setOnClickListener {
            finish()
        }
    }
}