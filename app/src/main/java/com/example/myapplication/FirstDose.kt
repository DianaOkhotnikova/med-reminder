package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.CheckBox
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.room.DbObjects
import com.example.myapplication.db.FirstDoseEnum

class FirstDose : AppCompatActivity()  {
    lateinit var button: ImageButton
    lateinit var morning: CheckBox
    lateinit var afternoon: CheckBox
    lateinit var evening: CheckBox
    lateinit var night: CheckBox
    lateinit var beforeFood: CheckBox
    lateinit var afterFood: CheckBox
    lateinit var anytime: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.first_dose)
        button = findViewById(R.id.btnName)
        morning = findViewById(R.id.morning)
        afternoon = findViewById(R.id.afternoon)
        evening = findViewById(R.id.evening)
        night = findViewById(R.id.night)
        beforeFood = findViewById(R.id.beforeFood)
        afterFood = findViewById(R.id.afterFood)
        anytime = findViewById(R.id.anytime)
        @Suppress("DEPRECATION")
        val objectExtra = intent.getSerializableExtra("objects") as DbObjects
        println(objectExtra)
        if (objectExtra.flagUpdate){
            val pars = objectExtra.firstdose.split("/")
            pars.forEach {
                when (it){
                    FirstDoseEnum.BeforeFood.toString() -> beforeFood.isChecked = true
                    FirstDoseEnum.AfterFood.toString() -> afterFood.isChecked = true
                    FirstDoseEnum.Anytime.toString() -> anytime.isChecked = true
                    FirstDoseEnum.Morning.toString() -> morning.isChecked = true
                    FirstDoseEnum.Afternoon.toString() -> afternoon.isChecked = true
                    FirstDoseEnum.Evening.toString() -> evening.isChecked = true
                    FirstDoseEnum.Night.toString() -> night.isChecked = true
                    else -> println("Error")
                }
            }
        }
        var firstDose = ""
        button.setOnClickListener {
            if (beforeFood.isChecked){
                firstDose = FirstDoseEnum.BeforeFood.toString()
            }
            if (afterFood.isChecked){
                firstDose = FirstDoseEnum.AfterFood.toString()
            }
            if (anytime.isChecked){
                firstDose =  FirstDoseEnum.Anytime.toString()
            }
            if (morning.isChecked){
                firstDose += "/" + FirstDoseEnum.Morning.toString()
            }
            if (afternoon.isChecked){
                firstDose += "/" + FirstDoseEnum.Afternoon.toString()
            }
            if (evening.isChecked){
                firstDose += "/" + FirstDoseEnum.Evening.toString()
            }
            if (night.isChecked){
                firstDose += "/" + FirstDoseEnum.Night.toString()
            }
            objectExtra.firstdose = firstDose
            val intent: Intent = Intent(
                this,
                SetTime::class.java
            )
            intent.putExtra("objects", objectExtra)
            startActivity(intent)
        }
        button = findViewById(R.id.imageButton11)
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                ChooseTheDays::class.java
            )
            intent.putExtra("objects", objectExtra)
            startActivity(intent)
        }
    }
}