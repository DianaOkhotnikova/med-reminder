package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.room.DbObjects
import com.google.android.material.textfield.TextInputEditText

class AddMedication : AppCompatActivity()  {
    lateinit var button: ImageButton
    lateinit var medicine1: TextInputEditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_medication)
        button = findViewById(R.id.btnName)
        var flag = false
        flag = intent.getBooleanExtra("editFlag", flag)
        println(flag)
        medicine1 = findViewById(R.id.medicine)
        var dbObjects = DbObjects()
        if(flag){
            @Suppress("DEPRECATION")
            dbObjects = intent.getSerializableExtra("obj") as DbObjects
            dbObjects.flagUpdate = true
            medicine1.setText(dbObjects.medicine)
        }
        button.setOnClickListener {
            dbObjects.medicine = medicine1.text.toString()
            val intent: Intent = Intent(
                this,
                ThreePageActivity::class.java
            )
            println(dbObjects)
            intent.putExtra("objects", dbObjects)
            startActivity(intent)
        }
        button = findViewById(R.id.imageButton11)
        button.setOnClickListener {
            val intent: Intent = Intent(
                this,
                HomeActivity::class.java
            )
            startActivity(intent)
        }

    }

}