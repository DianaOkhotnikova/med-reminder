package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputBinding
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.navigation.NavigationBarView
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {
    private lateinit var binding: InputBinding
    lateinit var button: ImageButton
    lateinit var textName: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) { 
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button = findViewById(R.id.btnName)

        val editor = getSharedPreferences("SETTING", MODE_PRIVATE)
        textName = findViewById(R.id.inputTextName)
        textName.setText(editor.getString("name", null))

        button.setOnClickListener{
            val editor = getSharedPreferences("SETTING", MODE_PRIVATE).edit()
            editor.putString("name", textName.text.toString())
            editor.apply()
            startActivity(Intent(this,HomeActivity::class.java))
        }
    }
}