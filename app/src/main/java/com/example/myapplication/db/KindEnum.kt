package com.example.myapplication.db

enum class KindEnum {
    Pill,
    Solution,
    Injection,
    Powder,
    Drops,
    Inhaler,
    Other
}