package com.example.myapplication.db

enum class FirstDoseEnum {
    Morning,
    Afternoon,
    Evening,
    Night,
    BeforeFood,
    AfterFood,
    Anytime
}