package com.example.myapplication.db

enum class OftenEnum {
    Daily,
    OnceAWeek,
    TwoDaysAWeek,
    ThreeDaysAWeek,
    FourDaysAWeek,
    FiveDaysAWeek,
    SixDaysAWeek,
    OnceAMonth,
    AlternateDays,
}