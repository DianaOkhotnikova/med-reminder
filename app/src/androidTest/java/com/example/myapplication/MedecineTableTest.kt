package com.example.myapplication

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.myapplication.room.GuideTable
import com.example.myapplication.room.MedecineDB
import com.example.myapplication.room.MedecineDao
import com.example.myapplication.room.MedecineTable
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertNull
import junit.framework.TestCase.assertTrue
import org.junit.After

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MedecineTableTest {

    private lateinit var db: MedecineDB
    private lateinit var dao: MedecineDao

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(context, MedecineDB::class.java).build()
        dao = db.medecineDao
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun testInsertMedecine() {
        val newMedecine = MedecineTable(medicine = "Rinvoq", kind = "Pill", strength = "400 mg", purpose = "Rheumatoid arthritis",
            often = "Daily", days = "Sunday", firstdose = "Morning Before Food", time = "8:00 AM")
        dao.insertDB(newMedecine)
        val insertedMedecine = dao.readTable().find { it.medicine == "Rinvoq" }
        assertNotNull(insertedMedecine.toString(), "The medicine has been added")
    }

    @Test
    fun testReadMedecine() {
        val medecineList = dao.readTable()
        assertTrue(medecineList.isNotEmpty().toString(), true)
    }

    @Test
    fun testUpdateMedecine() {
        val newMedecine = MedecineTable(medicine = "Rinvoq", kind = "Pill", strength = "400 mg", purpose = "Rheumatoid arthritis",
            often = "Daily", days = "Sunday", firstdose = "Morning Before Food", time = "8:00 AM")
        dao.insertDB(newMedecine)
        val insertedId = newMedecine.id+1
        dao.updateDB(insertedId, "Acetaminophen", "Solution", "40 g", "Rheumatic fever",
            "2 days a week", "Wednesday", "Afternoon After Food", "8:00 PM")
        val updatedMedecine = dao.readTable().find { it.id == insertedId }
        assertNotNull(updatedMedecine.toString(), "Found")
        assertEquals("Acetaminophen", updatedMedecine?.medicine)
    }

    @Test
    fun testDeleteMedecine() {
        val testMedecine = MedecineTable(medicine = "Rinvoq", kind = "Pill", strength = "400 mg", purpose = "Rheumatoid arthritis",
            often = "Daily", days = "Sunday", firstdose = "Morning Before Food", time = "8:00 AM")
        dao.insertDB(testMedecine)
        val insertedId = testMedecine.id+1
        dao.deleteDB(insertedId)
        assertTrue(dao.readTable().isEmpty())
    }

    @Test
    fun testInsertGuide() {
        val  newGuide = listOf(GuideTable("How to add new medicine ?", "raw/guide.mp4"))
        dao.insertGuide(newGuide)
        val insertedGuide = dao.readGuide().find { it.question == "How to add new medicine ?" }
        assertNotNull(insertedGuide.toString(), "Question added")
    }

    @Test
    fun testReadGuide() {
        val guideList = dao.readGuide()
        assertTrue(guideList.isNotEmpty().toString(), true)
    }

    @Test
    fun testUpdateGuide() {
        val newGuide = listOf(GuideTable("How to add new medicine ?", "raw/guide.mp4", 1))
        dao.insertGuide(newGuide)
        val insertedId = newGuide[0].id
        dao.updateGuide("How to share with others ?", "raw/guide1.mp4", insertedId)
        val updatedGuides = dao.readGuide().find { it.id == insertedId }
        assertNotNull(updatedGuides.toString(), "The table has been updated")
        assertEquals("How to share with others ?", updatedGuides?.question)
    }

    @Test
    fun testDeleteGuide() {
        val newGuide = listOf(GuideTable("How to add new medicine ?", "raw/guide.mp4", 1))
        dao.insertGuide(newGuide)
        val insertedId = newGuide[0].id
        dao.deleteGuide(insertedId)
        assertTrue(dao.readGuide().isEmpty())
    }

}